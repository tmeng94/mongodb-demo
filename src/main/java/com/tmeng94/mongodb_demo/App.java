package com.tmeng94.mongodb_demo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoClientOptions;

import com.mongodb.*;
import static com.mongodb.client.model.Filters.*;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.bson.Document;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
            }
        } };

        System.out.println("Please enter your MongoDB connection string: ");
        Scanner scanner = new Scanner(System.in);
        String connectionString = scanner.nextLine();
        scanner.close();

        try {
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(new KeyManager[0], trustAllCerts, new SecureRandom());
            // The second parameter allows additional options to be added to a connection
            // string
            MongoClientURI mongoClientURI = new MongoClientURI(connectionString,
                    MongoClientOptions.builder().sslContext(sslContext));

            MongoClient mongoClient = new MongoClient(mongoClientURI);
            // MongoClient mongoClient = new MongoClient("localhost", 27017);
            mongoClient.getDatabase("readyapi").getCollection("demo").find(eq("name", "hello"))
                    .forEach((Block<Document>) document -> {
                        System.out.println(document.toJson());
                    });
            mongoClient.close();
        } catch (final Exception ex) {
            ex.printStackTrace();
        }

    }
}
